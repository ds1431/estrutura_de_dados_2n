#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int hora;
    int minuto;
} Tempo;

typedef struct {
    Tempo chave;
    char *cidade;
} ParChaveValor;

int compararTempos(Tempo t1, Tempo t2) {
    if (t1.hora > t2.hora) return 1;
    if (t1.hora < t2.hora) return -1;
    if (t1.minuto > t2.minuto) return 1;
    if (t1.minuto < t2.minuto) return -1;
    return 0;
}

int main() {
    ParChaveValor cidades[] = {
        {{9, 0}, "Cidade A"},
        {{12, 30}, "Cidade B"},
        {{15, 45}, "Cidade C"},
        {{18, 0}, "Cidade D"},
        {{20, 30}, "Cidade E"}
    };

    int numCidades = sizeof(cidades) / sizeof(cidades[0]);

    printf("Digite o primeiro horario no formato HH:MM: ");
    Tempo horaUsuario1;
    scanf("%d:%d", &horaUsuario1.hora, &horaUsuario1.minuto);

    printf("Digite o segundo horario no formato HH:MM: ");
    Tempo horaUsuario2;
    scanf("%d:%d", &horaUsuario2.hora, &horaUsuario2.minuto);

    int comparacao = compararTempos(horaUsuario1, horaUsuario2);

    if (comparacao > 0) {
        printf("1\n");
        printf("Cidade correspondente ao primeiro horario: %s\n", cidades[comparacao].cidade);
    } else if (comparacao < 0) {
        printf("-1\n");
        printf("Cidade correspondente ao segundo horario: %s\n", cidades[-comparacao].cidade);
    } else {
        printf("0\n");
        printf("Cidades correspondentes aos horarios: %s\n", cidades[0].cidade);
    }

    return 0;
}
